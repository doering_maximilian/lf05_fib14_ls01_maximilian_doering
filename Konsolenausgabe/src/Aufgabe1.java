
public class Aufgabe1 {

	public static void main(String[] args) {

		String s1 = "Das ist ein Beispielsatz.";
		String s2 = "Ein Beispielsatz ist das.";
		
		System.out.print(s1 + " " + s2);
		System.out.println("");
		System.out.println("Das ist ein \"Beispielsatz\".");
		System.out.println("Ein Beispielsatz ist das.");
		System.out.printf( "%50s\n" , s1);
		System.out.printf( "%-50.12s\n" , s2);
		//print gibt immer in der gleichen Zeile, println macht einen Zeilenumbruch
		
		
	}

}
