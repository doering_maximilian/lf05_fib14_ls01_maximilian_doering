
public class Aufgabe1 {

	public static void main(String[] args) {
		
		System.out.print(berechneMittelwert(3, 5));

	}
	
	public static double berechneMittelwert(double x, double y) {
		
		return (x + y) / 2.0;
		
	}
		
}