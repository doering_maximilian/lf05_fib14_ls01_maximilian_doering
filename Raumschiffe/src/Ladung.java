public class Ladung {
	
	private String name = "";
	private int count = 0;
	
	Ladung () {}
	
	Ladung (String name, int count) {
		
		this.name = name;
		this.count = count;
		
	}
	
	public String getName () {
		return this.name; }
	
	public void setName (String name) {
		this.name = name; }
	
	public int getCount () {
		return this.count; }
	
	public void setCount (int count) {
		this.count = count; }
	
	@Override public String toString () {
		return this.name + " " + this.count; }
	
}