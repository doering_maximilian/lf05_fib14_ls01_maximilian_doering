import java.util.ArrayList;
import java.util.Random;

class Raumschiff {
	
	private String name = "rs";
	private int energyProduction = 100;
	private int lifesupport = 100;
	private int hull = 100;
	private int shield = 100;
	private int photonTorpedos = 10;
	private int repairAndroids = 10;
	private ArrayList<Ladung> cargoManifest;
	public ArrayList<String> broadcastCommunicator;
	
	Raumschiff () {
		
		this.cargoManifest = new ArrayList<Ladung>();
		this.broadcastCommunicator = new ArrayList<String>();
	
	}
	
	Raumschiff (String name, int energyProduction, int lifesupport, int hull,
			int shield, int photonTorpedos, int repairAndroids) {
		
		this.name = name;
		this.energyProduction = energyProduction;
		this.lifesupport = lifesupport;
		this.hull = hull;
		this.shield = shield;
		this.photonTorpedos = photonTorpedos;
		this.repairAndroids = repairAndroids;
		this.cargoManifest = new ArrayList<Ladung>();
		this.broadcastCommunicator = new ArrayList<String>();
	
	}
	
	public String getName () {
		return this.name; }
	
	public void setName (String name) {
		this.name = name; }
	
	public float getEnergyProduction () {
		return this.energyProduction; }
	
	public void setEnergyProduction (int energyProduction) {
	 	this.energyProduction = energyProduction; }
	
	public float getLifesupport () {
		return this.lifesupport; }
	
	public void setLifesupport (int lifesupport) {
		this.lifesupport = lifesupport; }
	
	public float getHull () {
		return this.hull; }
	
	public void setHull (int hull) {
		this.hull = hull; }
	
	public float getShield () {
		return this.shield; }
	
	public void setShield (int shield) {
		this.shield = shield; }
	
	public int getPhotonTorpedos () {
		return this.photonTorpedos; }
	
	public void setPhotonTorpedos (int photonTorpedos) {
		this.photonTorpedos = photonTorpedos; }
	
	public int getRepairAndroids () {
		return this.repairAndroids; }
	
	public void setRepairAndroids (int repairAndroids) {
		this.repairAndroids = repairAndroids; }
	
	// Ladung
	
	public void addCargo (String name, int count) {
		Ladung cargo = new Ladung(name, count);
		this.cargoManifest.add(cargo); }
	
	public Ladung getCargo (int i) {
		System.out.println(this.cargoManifest.get(i));
		return this.cargoManifest.get(i); }
	
	public void consoleCargo () {
		System.out.println("Das Schiff " + this.name + " hat folgende Ladung: ");
		for(int i = 0; i < this.cargoManifest.size(); i++) {
			System.out.println(this.getCargo(i)); } System.out.println(); }
	
	// Nachrichten
	
	public void consoleStatus () {
		System.out.println("Der Status des Schiffes " + this.name + ":");
		System.out.println("Unser Schiffsname " + this.name);
		System.out.println("Unsere Energie " + this.energyProduction);
		System.out.println("Unsere Lebenserhaltungssysteme " + this.lifesupport);
		System.out.println("Unsere H�lle " + this.hull);
		System.out.println("Unsere Schilde " + this.shield);
		System.out.println("Unsere Photonentorpedos " + this.photonTorpedos);
		System.out.println("Unsere Reparaturandroiden " + this.repairAndroids);
		System.out.println(); }
	
	public void broadcastMessage (String message) {
		this.broadcastCommunicator.add(message); }
	
	public String getMessage (int i) {
		return this.broadcastCommunicator.get(i); }
	
	public void consoleMessages () {
		for(int i = 0; i < this.broadcastCommunicator.size(); i++) {
			System.out.println(this.getMessage(i)); } }
	
	// Schie�en
	
	public void markHit(Raumschiff spaceShip) {
		System.out.println(spaceShip.name + "wurde getroffen!");
		spaceShip.shield = spaceShip.shield - 50;
		if(spaceShip.shield <= 0) {
			spaceShip.hull = spaceShip.hull - 50;
			spaceShip.energyProduction = spaceShip.energyProduction - 50;
			if(spaceShip.hull <= 0) {
				spaceShip.broadcastMessage("Unsere Lebenserhaltungssysteme wurden vernichtet!"); } } }
	
	public void shootPhotonTorpedos(Raumschiff spaceShip) {
		if(this.photonTorpedos > 0) {
			this.photonTorpedos = this.photonTorpedos - 1;
			this.broadcastMessage("Photonentorpedos abgeschossen");
			this.markHit(spaceShip); }
		else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.broadcastMessage("-=*Click*=-"); }
		System.out.println(); }
	
	public void shootPhaserCanons(Raumschiff spaceShip) {
		if(this.energyProduction >= 50) {
			this.photonTorpedos = this.energyProduction - 50;
			this.broadcastMessage("Phaserkanonen abgeschossen");
			this.markHit(spaceShip); }
		else { System.out.println("-=*Click*=-"); }
		System.out.println(); }
	
	// Reparatur
	
	public void repairShip(boolean en, boolean li, boolean hu, boolean sh, int androids) {
		if(androids > this.repairAndroids) { androids = this.repairAndroids; }
		int structures = 0;
		if(en=true) { structures = structures + 1; }
		if(li=true) { structures = structures + 1; }
		if(hu=true) { structures = structures + 1; }
		if(sh=true) { structures = structures + 1; }
		Random rand = new Random();
		int rand_int = rand.nextInt(101);
		int procent = (rand_int * androids) / structures;
		if(en=true) { this.energyProduction = this.energyProduction + procent; }
		if(li=true) { this.lifesupport = this.lifesupport + procent; }
		if(hu=true) { this.hull = this.hull + procent; }
		if(sh=true) { this.shield = this.shield + procent; }
		this.repairAndroids = this.repairAndroids - androids; }
	
	
	// Static Void
	
	public static void main(String[] args) {
		
		//Raumschiff xyz = new Raumschiff("name", energy, life,
		//hull, shield, torpedos, repair)
		Random rand = new Random();
		String messages = new String();
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 0, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", rand.nextInt(101), 100, rand.nextInt(101), rand.nextInt(101), 3, 5);
		
		klingonen.addCargo("Ferengi Schneckensaft", 200);
		klingonen.addCargo("Bat'leth Klingonen Schwert", 200);
		romulaner.addCargo("Borg-Schrott", 5);
		romulaner.addCargo("Rote Materie", 2);
		romulaner.addCargo("Plasma-Waffe", 50);
		vulkanier.addCargo("Forschungssonde", 35);
		klingonen.shootPhotonTorpedos(romulaner);
		romulaner.shootPhaserCanons(klingonen);
		vulkanier.broadcastMessage("Gewalt ist nicht logisch");
		klingonen.consoleStatus(); klingonen.consoleCargo();
		vulkanier.repairShip(true, true, true, true, vulkanier.repairAndroids);
		// vulkanier.replenishCargo; vulkanier.cleanCargo;
		klingonen.shootPhotonTorpedos(romulaner); klingonen.shootPhotonTorpedos(romulaner);
		klingonen.consoleStatus(); klingonen.consoleCargo();
		romulaner.consoleStatus(); romulaner.consoleCargo();
		vulkanier.consoleStatus(); vulkanier.consoleCargo();
		romulaner.consoleMessages();
		
	}
	
}