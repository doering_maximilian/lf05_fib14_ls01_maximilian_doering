import java.util.Scanner;
import java.lang.Math.*;

public class Aufgabe6 {
	
    public static void main(String[] args) {
    	
    	Scanner input = new Scanner(System.in);
    	
    	double e = 2.718;
    	
    	double x = input.nextDouble();
    	
    	if (x <= 0) {
    		
    		x = Math.pow(e, x);
    		System.out.printf("exponentiell; " + x);
    		
    	}
    	
    	else if (0 < x || x <= 3) {
    		
    		x = Math.pow(x, 2) + 1;
    		System.out.printf("quadratisch; " + x);    		
    		
    	}
    	
    	else {
    		
    		x = 2 * x + 4;
    		System.out.printf("linear; " + x);
    		
    	}
    	
    	
    }
    
}