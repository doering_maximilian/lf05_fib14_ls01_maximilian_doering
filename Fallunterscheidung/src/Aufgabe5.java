import java.util.Scanner;

public class Aufgabe5 {
	
    public static void main(String[] args) {
    	
    	Scanner input = new Scanner(System.in);
    	System.out.println("Welchen Wert m�chten Sie berechnen? U, R oder I?");
    	char auswahl = input.next().charAt(0);
    	double wert1;
    	double wert2;
    	double ergebnis = 0;
    	
    	
    	if (auswahl == 'U' || auswahl == 'u') {
    		
    		System.out.println("Welchen Wert soll R haben?");
    		wert1 = input.nextDouble();
    		System.out.println("Welchen Wert soll I haben?");
    		wert2 = input.nextDouble();
    		ergebnis = wert1 * wert2;
    		
    	}
    	
    	else if (auswahl == 'R' || auswahl == 'r') {
    		
    		System.out.println("Welchen Wert soll I haben?");
    		wert1 = input.nextDouble();
    		System.out.println("Welchen Wert soll U haben?");
    		wert2 = input.nextDouble();
    		ergebnis = wert2 / wert1;
    		
    	}
    	
    	else if (auswahl == 'I' || auswahl == 'i') {
    		
    		System.out.println("Welchen Wert soll U haben?");
    		wert1 = input.nextDouble();
    		System.out.println("Welchen Wert soll R haben?");
    		wert2 = input.nextDouble();
    		ergebnis = wert1 / wert2;
    		
    	}
    	
		System.out.printf("Das Ergebnis ist: " + ergebnis);
    	
    }

}