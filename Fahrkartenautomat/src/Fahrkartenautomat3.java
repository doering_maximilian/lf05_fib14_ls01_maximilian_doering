import java.util.Scanner;

public class Fahrkartenautomat3 {
	
    public static void main(String[] args) {
    	
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	
    	rueckgeldAusgeben(fahrkartenBezahlen(zuZahlenderBetrag), zuZahlenderBetrag);
    	
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
    	Scanner input = new Scanner(System.in);
    	
    	double ticketpreis = 2.5;

        System.out.printf("Ticketpreis (Euro): %.2f", ticketpreis);
        System.out.println();
        System.out.print("Anzahl der Tickets: ");
        int ticketanzahl = input.nextInt();
        return ticketanzahl * ticketpreis;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner input = new Scanner(System.in);
    
    	double eingezahlterGesamtbetrag = 0.0;
    	    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	   System.out.printf("Noch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.println();
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = input.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
            
        }
    	
    	fahrkartenAusgeben();
    	
    	return eingezahlterGesamtbetrag;
    	
    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
     	   System.out.println();
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	muenzeAusgeben(2, "Euro");
         	  	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, "Euro");
         	  	r�ckgabebetrag -= 1.0;	
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, "Cent");
         	  	r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, "Cent");
         	  	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, "Cent");
 	          	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, "Cent");
            	r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, Ihre(n) Fahrschein(e)\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");    	
     
    }
    
    public static void warte(int millisekunden) {
    	
    	   try {
    			Thread.sleep(millisekunden);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	
    	if (einheit == "Euro") {
    		System.out.println(betrag + " Euro");
    	}
    	else if (einheit == "Cent") {
    		System.out.println(betrag + " Cent");
    	}
    	
    }
      
}
