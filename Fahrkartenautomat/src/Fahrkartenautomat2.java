import java.util.Scanner;

public class Fahrkartenautomat2
{
    public static void main(String[] args)
    {
       Scanner input = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0;
       double eingezahlterGesamtbetrag = 0;
       double eingeworfeneM�nze = 0;
       double r�ckgabebetrag = 0;
       double ticketpreis = 2.5;
       int ticketanzahl = 0;
       

       System.out.printf("Ticketpreis (Euro): %.2f", ticketpreis);
       System.out.println();
       System.out.print("Anzahl der Tickets: ");
       ticketanzahl = input.nextInt();
       zuZahlenderBetrag = ticketanzahl * ticketpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.println();
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = input.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
           
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
    	   System.out.println();
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;	
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, Ihre(n) Fahrschein(e)\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}

// zu 5.
// Ich habe den Datentyp Integer verwendet, da dieser �blicherweise f�r Ganzzahlen verwendet wird
// (einen Bruchteil eines Tickets zu drucken w�re nicht sinnvoll). Man k�nnte auch einen kleineren
// Datentyp verwenden (um effizienter zu arbeiten), welcher ebenfalls f�r Ganzzahlen gedacht ist, wie z.B. shorts oder bytes. 
// Man sollte den potenziellen Fahrgast aber nicht daran hindern, das Verkehrsunternehmen um eine Millionen Euro zu bereichern.


// zu 6.
// Der vorgebene Betrag des Ticketpreises wird multipliziert mit der selbstw�hlbaren Ticketanzahl.
// Der Integer wird dabei kompatibel zu dem Double gemacht, indem er konviert wird.