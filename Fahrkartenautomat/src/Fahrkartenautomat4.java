import java.util.Scanner;

public class Fahrkartenautomat4 {
	
    public static void main(String[] args) {
    	
    	while (true) {
    		
    	System.out.println("\n\n\n\n\n"+"========================================================"+"\n"+
    	"Fahrkartenbestellvorgang:\n\n"+
    	"W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n"+
    	"Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"+
    	"Tageskarte Regeltarif AB [8,60 EUR] (2)\n"+
    	"Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"+
    	"Bezahlen (9);");
    	
    		
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	
    	rueckgeldAusgeben(fahrkartenBezahlen(zuZahlenderBetrag), zuZahlenderBetrag);
    	
    	}
    	
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
    	@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
    	boolean eingabe = false;    	
    	double ticketpreis=0;
    	int ticketanzahl = 0;
    	int ticketwahl = 9;
    	int gesamtTicketanzahl = 0;
    	double gesamtTicketpreis = 0;
    	
    	while (eingabe == false) {
    	
    	while (eingabe == false) {
    		
    		System.out.print("\nIhre Wahl: ");
    		ticketwahl = input.nextInt();
    		if (ticketwahl == 1) { ticketpreis = 2.9; eingabe = true; }
    		else if (ticketwahl == 2) { ticketpreis = 8.6; eingabe = true; }
    		else if (ticketwahl == 3) { ticketpreis = 23.5; eingabe = true; }
    		else if (ticketwahl == 9) { ticketpreis = 0; eingabe = true; }
    		else { System.out.println("\nBitte w�hlen Sie eine g�ltige Ticketnummer!");}
    		
    		}
    	
    	eingabe = false;
        
        while (eingabe == false) {
        	
        	if (ticketpreis != 0) {
        	System.out.printf("\nTicketpreis (Euro): %.2f", ticketpreis);
            System.out.print("\nAnzahl der Tickets: ");
        	ticketanzahl = input.nextInt();
        	if (ticketanzahl > 10) {
        	System.out.println("\nDie Gesamtticketanzahl pro Auftrag darf 10 nicht �berschreiten.\n\n"); }
        	else if (ticketanzahl < 0) {
        	System.out.println("\nBitte w�hlen Sie eine g�ltige Ticketanzahl.\n\n"); }
        	else {eingabe = true; } }
        	if (ticketpreis == 0) {eingabe = true;}
        
    	}
        
        gesamtTicketpreis = gesamtTicketpreis + (ticketanzahl * ticketpreis);
        gesamtTicketanzahl = gesamtTicketanzahl + ticketanzahl;
        if (gesamtTicketanzahl > 10) {gesamtTicketanzahl = gesamtTicketanzahl - ticketanzahl;
        	System.out.println("Die Gesamtticketanzahl pro Auftrag darf 10 nicht �berschreiten.\n\n"); }
        else if (gesamtTicketanzahl != 0) {
        	System.out.printf("\nIhre derzeitige Ticketanzahl betr�gt: " + gesamtTicketanzahl);
        	System.out.printf("\nIhr derzeitiger Gesamtbetrag betr�gt: " + gesamtTicketpreis + "0 Euro\n"); }
        else if (gesamtTicketanzahl == 0 ) {
        	System.out.println("\nVor der Ausgabe m�ssen Sie mindestens ein Ticketausw�hlen."); }
        eingabe = false;
        if (ticketwahl == 9 && gesamtTicketanzahl != 0) {eingabe = true;}

        
    	}
    	
        return gesamtTicketpreis;
        
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
    
    	double eingezahlterGesamtbetrag = 0.0;
    	    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	   System.out.printf("\nNoch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.println();
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = input.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
            
        }
    	
    	fahrkartenAusgeben();
    	
    	return eingezahlterGesamtbetrag;
    	
    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
     	   System.out.println();
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	muenzeAusgeben(2, "Euro");
         	  	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, "Euro");
         	  	r�ckgabebetrag -= 1.0;	
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, "Cent");
         	  	r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, "Cent");
         	  	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, "Cent");
 	          	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, "Cent");
            	r�ckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, Ihre(n) Fahrschein(e)\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");    	
     
    }
    
    public static void warte(int millisekunden) {
    	
    	   try {
    			Thread.sleep(millisekunden);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	
    	if (einheit == "Euro") {
    		System.out.println(betrag + " Euro");
    	}
    	else if (einheit == "Cent") {
    		System.out.println(betrag + " Cent");
    	}
    	
    }
      
}
