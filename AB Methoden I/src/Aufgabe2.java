
public class Aufgabe2 {

	public static void main(String[] args) {
		
		System.out.println(multiplizieren(2.36, 7.87));
		
	}

	public static double multiplizieren(double wert1, double wert2) {
		
		return wert1 * wert2;

	}
}