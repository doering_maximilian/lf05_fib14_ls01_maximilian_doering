
import java.lang.Math;

public class Aufgabe4 {

	public static void main(String[] args) {
		
		System.out.println(wuerfel(2));
		System.out.println(quader(1, 2, 3));
		System.out.println(pyramide(2, 3));
		System.out.println(kugel(3));
		
	}
	
	public static double wuerfel(double a) {
		
		return Math.pow(a, 3);
	
	}
	
	public static double quader(double a, double b, double c) {
		
		return a * b * c;
	
	}
	
	public static double pyramide(double a, double h) {
		
		return a * a * h / 3;
	
	}
	
	public static double kugel(double r) {
		
		return 4 / 3 * Math.pow(r, 3) * Math.PI;
	
	}
	
}